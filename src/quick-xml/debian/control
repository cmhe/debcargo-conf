Source: rust-quick-xml
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-memchr-2+default-dev (>= 2.3.4-~~) <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Robin Krahl <robin.krahl@ireas.org>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/quick-xml]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/quick-xml
X-Cargo-Crate: quick-xml
Rules-Requires-Root: no

Package: librust-quick-xml-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-memchr-2+default-dev (>= 2.3.4-~~)
Suggests:
 librust-quick-xml+encoding-rs-dev (= ${binary:Version}),
 librust-quick-xml+serde-dev (= ${binary:Version})
Provides:
 librust-quick-xml+default-dev (= ${binary:Version}),
 librust-quick-xml+escape-html-dev (= ${binary:Version}),
 librust-quick-xml-0-dev (= ${binary:Version}),
 librust-quick-xml-0+default-dev (= ${binary:Version}),
 librust-quick-xml-0+escape-html-dev (= ${binary:Version}),
 librust-quick-xml-0.22-dev (= ${binary:Version}),
 librust-quick-xml-0.22+default-dev (= ${binary:Version}),
 librust-quick-xml-0.22+escape-html-dev (= ${binary:Version}),
 librust-quick-xml-0.22.0-dev (= ${binary:Version}),
 librust-quick-xml-0.22.0+default-dev (= ${binary:Version}),
 librust-quick-xml-0.22.0+escape-html-dev (= ${binary:Version})
Description: High performance xml reader and writer - Rust source code
 This package contains the source for the Rust quick-xml crate, packaged by
 debcargo for use with cargo and dh-cargo.

Package: librust-quick-xml+encoding-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-quick-xml-dev (= ${binary:Version}),
 librust-encoding-rs-0.8+default-dev
Provides:
 librust-quick-xml+encoding-rs-dev (= ${binary:Version}),
 librust-quick-xml-0+encoding-rs-dev (= ${binary:Version}),
 librust-quick-xml-0+encoding-dev (= ${binary:Version}),
 librust-quick-xml-0.22+encoding-rs-dev (= ${binary:Version}),
 librust-quick-xml-0.22+encoding-dev (= ${binary:Version}),
 librust-quick-xml-0.22.0+encoding-rs-dev (= ${binary:Version}),
 librust-quick-xml-0.22.0+encoding-dev (= ${binary:Version})
Description: High performance xml reader and writer - feature "encoding" and 1 more
 This metapackage enables feature "encoding" for the Rust quick-xml crate, by
 pulling in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "encoding_rs" feature.

Package: librust-quick-xml+serde-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-quick-xml-dev (= ${binary:Version}),
 librust-serde-1+default-dev
Provides:
 librust-quick-xml+serialize-dev (= ${binary:Version}),
 librust-quick-xml-0+serde-dev (= ${binary:Version}),
 librust-quick-xml-0+serialize-dev (= ${binary:Version}),
 librust-quick-xml-0.22+serde-dev (= ${binary:Version}),
 librust-quick-xml-0.22+serialize-dev (= ${binary:Version}),
 librust-quick-xml-0.22.0+serde-dev (= ${binary:Version}),
 librust-quick-xml-0.22.0+serialize-dev (= ${binary:Version})
Description: High performance xml reader and writer - feature "serde" and 1 more
 This metapackage enables feature "serde" for the Rust quick-xml crate, by
 pulling in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "serialize" feature.
